#!/bin/bash
# script to select video and audio and download them from youtube
# check if yt-dlp is installed
type yt-dlp &> /dev/null || { echo ❌ yt-dlp is not installed 1>&2; exit 69; }
# check if fzf is installed
type fzf &> /dev/null || { echo ❌ fzf is not installed 1>&2; exit 69; }
url="${1}"
# check if url was specified
[[ -n "$url" ]] || { echo ❌ no url specified 1>&2; exit 69; }
# get media information
info=$(yt-dlp --cookies-from-browser brave -F "$url")
# select videoformat
video="$(fzf --no-sort --reverse --prompt='select video file 👉 ' --pointer='👉' <<<"$info")"
# cut out first element
videoformat="${video%% *}"
#echo $videoformat
# select audioformat
audio="$(fzf --no-sort --reverse --prompt='select audio file 👉 ' --pointer='👉' <<<"$info")"
# cut out first element
audioformat="${audio%% *}"
#echo $audioformat
# check if videoformat was selected and download
[[ -n "$videoformat" ]] && { echo ⬇️ downloading 🎬video file; yt-dlp --cookies-from-browser brave  -f "$videoformat" "$url"; }
# check if audioformat was selected and download
[[ -n "$audioformat" ]] && { echo ⬇️ downloading 🎵audio file; yt-dlp --cookies-from-browser brave  -f "$audioformat" "$url"; }
