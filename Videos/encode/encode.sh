#!/bin/bash
# script to encode videofiles from 10bit to 8bit with ffmpeg
for inputfile in *.mkv; do
  #echo ${inputfile} ${inputfile/-Tanuki.x.ABJ/.8bit}
  ffmpeg -i ${inputfile} -c:v libx264 -crf 18 -vf format=yuv420p -c:a copy ${inputfile/-Tanuki.x.ABJ/.8bit}
done
