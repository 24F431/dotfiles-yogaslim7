#!/bin/bash
# script to merge videofile and audiofile with ffmpeg
# ffmpeg -i 'vieofile' -i 'audiofile' -c:v copy -c:a copy -strict experimental 'outputfile'
# check if fzf is installed
type fzf &>/dev/null || {
  echo ❌ fzf is not installed 1>&2
  exit 69
}
video="$(ls -1 *.mp4 *.webm 2>/dev/null | fzf --prompt='select video file 👉 ' --pointer='👉')"
# check if videofile was selected
[[ -n "$video" ]] || {
  echo ❌ no videofile selected 1>&2
  exit 69
}
echo 'video🎬='$video
audio="$(ls -1 *.m4a *.webm 2>/dev/null | fzf --prompt='select audio file 👉 ' --pointer='👉')"
# check if audiofile was selected
[[ -n "$audio" ]] || {
  echo ❌ no audiofile selected 1>&2
  exit 69
}
echo 'audio🎵='$audio
outputfile="$(sed 's/^v//' <<<$video | sed 's/ \?\[[^\.]\+//')"
#outputfile="$(sed 's/^v//' <<<$video | sed 's/-[^- ]\+\././')"
echo 'outputfile='$outputfile
# check if videofile equals outputfile
[[ "$video" == "$outputfile" ]] && {
  echo ❌ videofile equals outputfile 1>&2
  exit 69
}
# merge videofile and audiofile
ffmpeg -i "$video" -i "$audio" -c:v copy -c:a copy -strict experimental "$outputfile"
