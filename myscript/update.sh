#!/bin/bash
# script for system update
#                  _       _       
#  _   _ _ __   __| | __ _| |_ ___ 
# | | | | '_ \ / _` |/ _` | __/ _ \
# | |_| | |_) | (_| | (_| | ||  __/
#  \__,_| .__/ \__,_|\__,_|\__\___|
#       |_|                        

figlet system$'\n'update | /usr/games/lolcat
sudo -- bash -c 'apt update && apt list --upgradeable && apt upgrade -y && apt clean' \
  && { kdialog --passivepopup '✅ update ok' 6; figlet 'update ok' | /usr/games/lolcat; } \
  || { kdialog --passivepopup '❌ update fail' 6; figlet 'update fail' | /usr/games/lolcat; }
