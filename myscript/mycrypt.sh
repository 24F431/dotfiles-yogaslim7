#!/bin/bash
# script mounts cryfs folder
GREEN='\033[0;32m'
NC='\033[0m' # No Color
# check if folder is mounted
if mount | grep 'cryfs@/home/raf/crypt/encrypted/ on /home/raf/crypt/decrypted' --quiet; then
	echo -e "${GREEN}\$ cryfs-unmount \"/home/raf/crypt/decrypted/\"${NC}"
	cryfs-unmount "/home/raf/crypt/decrypted/"
else
	echo -e "${GREEN}\$ cryfs ~/crypt/encrypted/ ~/crypt/decrypted/${NC}"
	cryfs ~/crypt/encrypted/ ~/crypt/decrypted/
fi
