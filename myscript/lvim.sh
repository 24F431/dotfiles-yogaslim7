#!/bin/bash
# script starts lazyvim in a docker container
lvim_status=$(docker ps --filter name=mylazyvim | wc -l)
if [[ $lvim_status = "2" ]]; then
	echo lazy vim is running, attaching to it
	docker attach mylazyvim
elif [[ $lvim_status = "1" ]]; then
	echo lazy vim is not running, starting it
	docker start mylazyvim && docker attach mylazyvim
else
	echo "error! unknown status $lvim_status" 1>&2
	exit 69
fi
