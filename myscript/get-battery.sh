#!/bin/bash
# battery status script for tint2 panel
# check if acpi is installed
type acpi &> /dev/null || { echo ❌ acpi is not installed 1>&2; exit 69; }
# declare icon variables
BAT_ICON_100=""
BAT_ICON_90=""
BAT_ICON_80=""
BAT_ICON_70=""
BAT_ICON_60=""
BAT_ICON_50=""
BAT_ICON_40=""
BAT_ICON_30=""
BAT_ICON_20=""
BAT_ICON_10=""
BAT_ICON_05=""
BAT_ICON_CHAR_100=" "
BAT_ICON_CHAR_90=" "
BAT_ICON_CHAR_80=" "
BAT_ICON_CHAR_60=" "
BAT_ICON_CHAR_40=" "
BAT_ICON_CHAR_30=" "
BAT_ICON_CHAR_20=" "
BAT_ICON_CHAR_10=""
# get information from battery x and cut out the name
myIFS="$IFS"
IFS=$'\n'
for x in $(acpi -b); do
  BAT=$x
done
BAT1=$(echo $BAT | sed "s|^Battery [01]: ||")
# change filed seperator
IFS=", "
# put battery data in an array
read -ra DATA <<<"${BAT1}"
# get state
STATE="${DATA[0]}"
# get percentage without unit
PER="${DATA[1]%\%*}"
# get time and cut out seconds
TIME="${DATA[2]%:*}"
# check battery state
BAT_ICON=
if [[ $STATE = "Discharging" ]]; then
  if [[ $PER -ge 97 ]]; then
    BAT_ICON=$BAT_ICON_100
  elif [[ $PER -le 5 ]]; then
    BAT_ICON=$BAT_ICON_05
  elif [[ $PER -lt 20 ]]; then
    BAT_ICON=$BAT_ICON_10
  elif [[ $PER -lt 30 ]]; then
    BAT_ICON=$BAT_ICON_20
  elif [[ $PER -lt 40 ]]; then
    BAT_ICON=$BAT_ICON_30
  elif [[ $PER -lt 50 ]]; then
    BAT_ICON=$BAT_ICON_40
  elif [[ $PER -lt 60 ]]; then
    BAT_ICON=$BAT_ICON_50
  elif [[ $PER -lt 70 ]]; then
    BAT_ICON=$BAT_ICON_60
  elif [[ $PER -lt 80 ]]; then
    BAT_ICON=$BAT_ICON_70
  elif [[ $PER -lt 90 ]]; then
    BAT_ICON=$BAT_ICON_80
  elif [[ $PER -lt 97 ]]; then
    BAT_ICON=$BAT_ICON_90
  fi
elif [[ $STATE = "Charging" ]]; then
  if [[ $PER -ge 97 ]]; then
    BAT_ICON=$BAT_ICON_CHAR_100
  elif [[ $PER -lt 20 ]]; then
    BAT_ICON=$BAT_ICON_CHAR_10
  elif [[ $PER -lt 30 ]]; then
    BAT_ICON=$BAT_ICON_CHAR_20
  elif [[ $PER -lt 40 ]]; then
    BAT_ICON=$BAT_ICON_CHAR_30
  elif [[ $PER -lt 60 ]]; then
    BAT_ICON=$BAT_ICON_CHAR_40
  elif [[ $PER -lt 80 ]]; then
    BAT_ICON=$BAT_ICON_CHAR_60
  elif [[ $PER -lt 90 ]]; then
    BAT_ICON=$BAT_ICON_CHAR_80
  elif [[ $PER -lt 97 ]]; then
    BAT_ICON=$BAT_ICON_CHAR_90
  fi
elif [[ $STATE = "Unknown" ]]; then
  echo " <span size=\"x-large\"></span> ${DATA[0]} "
  exit 0
elif [[ $STATE = "Full" ]]; then
  BAT_ICON=$BAT_ICON_100
  echo " $BAT_ICON${DATA[1]} "
  exit 0
fi
# output the state
echo " $BAT_ICON${DATA[1]} $TIME "
