#!/bin/bash
# script gets BTC and ETH course
#  ____ _____ ____   _____ _____ _   _ 
# | __ )_   _/ ___| | ____|_   _| | | |
# |  _ \ | || |     |  _|   | | | |_| |
# | |_) || || |___  | |___  | | |  _  |
# |____/ |_| \____| |_____| |_| |_| |_|
#                                      
BTC=
ETH=
VALUEONLY=
TRUNC=
ERROR=
HELP=
# get switches
while getopts ":betvh" opt; do
  case $opt in
    b) BTC=1 ;; # print BTC course
    e) ETH=1 ;; # print ETH course
    v) VALUEONLY=1 ;; # print values only
    t) TRUNC=1 ;; # truncate values
    h) HELP=1 ;; # print help
    ?) ERROR=1 ;; # Handle error: unknown option or missing required argument.
  esac
done
# if wrong switch or no switch is passed as argument
if [[ ! -z "$ERROR" ]] || [[ $OPTIND -eq 1 ]]; then
  printf "Usage: %s: [-b] [-e] [-v] [-t] [-h]\n" $0
  exit 69
fi
if [[ ! -z "$HELP" ]]; then
  printf "Usage: %s: [-b] [-e] [-v] [-t] [-h]\n" $0
  exit 0
fi
if [[ ! -z "$BTC" ]]; then
  btc=$(curl -s https://api.coinbase.com/v2/prices/btc-usd/spot -H 'CB-VERSION: 2015-04-08' | jq -r ".data.amount")
  if [[ ! -z "$TRUNC" ]]; then
    btc=$(python3 -c "print('{:.0f}'.format($btc))")
  fi
  if [[ ! -z "$VALUEONLY" ]]; then
    echo $btc
  else
    echo " $btc"
  fi
fi
if [[ ! -z "$ETH" ]]; then
  eth=$(curl -s https://api.coinbase.com/v2/prices/eth-usd/spot -H 'CB-VERSION: 2015-04-08' | jq -r ".data.amount")
  if [[ ! -z "$TRUNC" ]]; then
    eth=$(python3 -c "print('{:.0f}'.format($eth))")
  fi
  if [[ ! -z "$VALUEONLY" ]]; then
    echo $eth
  else
    echo "ﲹ $eth"
  fi
fi
