#!/bin/bash
#            _
#   ___ ___ | | ___  _ __
#  / __/ _ \| |/ _ \| '__|
# | (_| (_) | | (_) | |
#  \___\___/|_|\___/|_|
#                _       _
#  ___  ___ _ __(_)_ __ | |_ ___
# / __|/ __| '__| | '_ \| __/ __|
# \__ \ (__| |  | | |_) | |_\__ \
# |___/\___|_|  |_| .__/ \__|___/
#                 |_|
# chooses one color script
cat <<EOF | fzf --pointer='👉' | xargs -r -i bash -c ~/myscript/color/{}
colorwheel.sh
crunch.sh
darthvader.sh
dna.sh
ghosts.sh
guns.sh
illumina.sh
jangofett.sh
pacman.sh
space-invaders.sh
tanks.sh
tiefighter.sh
tux.sh
EOF
