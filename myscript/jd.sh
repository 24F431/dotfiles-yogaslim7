#!/bin/sh
# script to start JDownloader
#      _ ____                      _                 _           
#     | |  _ \  _____      ___ __ | | ___   __ _  __| | ___ _ __ 
#  _  | | | | |/ _ \ \ /\ / / '_ \| |/ _ \ / _` |/ _` |/ _ \ '__|
# | |_| | |_| | (_) \ V  V /| | | | | (_) | (_| | (_| |  __/ |   
#  \___/|____/ \___/ \_/\_/ |_| |_|_|\___/ \__,_|\__,_|\___|_|   
#
echo JDownloader starten
/bin/sh $HOME/jd2/JDownloader2 &
notify-send '✅ JDownloader starten'
