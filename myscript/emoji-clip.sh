#!/bin/bash
#                       _ _ 
#   ___ _ __ ___   ___ (_|_)
#  / _ \ '_ ` _ \ / _ \| | |
# |  __/ | | | | | (_) | | |
#  \___|_| |_| |_|\___// |_|
#                    |__/   
#
# script to copy emojis to clipboard

# check if rofi is installed
type rofi &> /dev/null || { echo rofi nicht gefunden 1>&2; exit 4711; }

# check if xclip is installed
type xclip &> /dev/null || { echo xclip nicht gefunden 1>&2; exit 4711; }

# check if file exists
[[ -f "$HOME/myscript/emoji.txt" ]] || { echo emoji.txt nicht vorhanden 1>&2; \
  exit 0815; }

# get emoji from file with dmenu
emoji=$(sed '/^#/d' $HOME/myscript/emoji.txt | cut -d ';' -f1 | \
  rofi -dmenu -i -l 20 | sed "s/ .*//")

# exit if empty
[[ -z "$emoji" ]] && exit 69

# copy emoji to clipboard
printf $emoji | xclip -selection c && notify-send "$emoji wurde kopiert"
