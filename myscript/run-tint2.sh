#!/bin/bash
sleep 5
xrandr -q | grep --quiet --extended-regexp "^HDMI-A-0 connected" \
  && { echo 'starting tint2'; notify-send '✅ starting tint2'; tint2; exit 0; } \
  || { echo 'HDMI-A-0 is disconnected'; }
