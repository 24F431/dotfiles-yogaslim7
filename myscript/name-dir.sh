#!/bin/bash
# script replace . with space in filename and copy it
# check if xclip is installed
type xclip &> /dev/null || { echo xclip nicht gefunden 1>&2; exit 4711; }
# check if fzf is installed
type fzf &> /dev/null || { echo fzf nicht gefunden 1>&2; exit 4711; }
# pipe content to fzf sub . and copy it
/bin/ls -1 | fzf | tr '.' ' ' | tr -d '\n' | xclip -selection clipboard
