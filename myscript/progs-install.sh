#!/bin/bash
# installs all my used programs after a distro upgrade
sudo apt install -y eza \
	highlight ranger speedometer nmap \
	git \
	doublecmd-gtk \
	htop \
	neofetch \
	lolcat \
	figlet \
	tmux wofi \
	bat \
	fonts-noto-color-emoji \
	fzf \
	mpv \
	openssh-server \
	vlc \
	curl \
	progress \
	neovim \
	ripgrep \
	qemu-kvm virt-manager ebtables bridge-utils \
	pamix \
	pv \
	sqlitebrowser \
	pandoc \
	imagemagick \
	kitty \
	fish \
	gparted \
	qrencode \
	iotop \
	zim \
	obs-studio \
	build-essential \
	usbtop \
	hexedit \
	ncal \
	cpufetch \
	btop \
	audacity \
	duf \
	cbonsai \
	tty-clock \
	cava \
	cmatrix \
	filezilla \
	breeze-cursor-theme \
	wireguard \
	resolvconf \
	acpi \
	net-tools \
	cryfs \
	distrobox \
	wl-clipboard \
	fonts-font-awesome \
	waybar \
	bmon \
  neovim-qt \
  pavucontrol-qt \
  ktorrent
