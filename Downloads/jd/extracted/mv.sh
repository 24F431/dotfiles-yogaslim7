#!/bin/bash
# move downloaded files to specified folder
# read source and destination from file
sed '/^#/d' ./source_dest | while read source dest; do
  # check if line is not empty
  if [[ -n "$source" ]] && [[ -n "$dest" ]]; then
    # echo source "$source"* dest "$dest"
    # move files
    mv -v -t "$dest" "$source"* &&
      notify-send "✅ move to $dest" ||
      notify-send "❌ move to $dest"
  fi
done

