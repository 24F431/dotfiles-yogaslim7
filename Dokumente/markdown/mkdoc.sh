#!/bin/bash
# HTML aus README.md erstellen
pandoc --self-contained --toc -c github-pandoc.css dotiles_bare.md -o dotiles_bare.html &&
    echo ✅ README.html erstellt ||
    echo ❌ README.html nicht erstellt 1>&2
