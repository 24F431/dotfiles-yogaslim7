# config file for fish shell
#   __ _     _     
#  / _(_)___| |__  
# | |_| / __| '_ \ 
# |  _| \__ \ | | |
# |_| |_|___/_| |_|
# turn greeting off
set fish_greeting
### custom prompt
#function fish_prompt
#    # Save our status
#    set -l last_pipestatus $pipestatus
#    set -lx __fish_last_status $status # Export for __fish_print_pipestatus.
#    set -l mystatus (__fish_print_pipestatus "[" "]" "|" \
#    (set_color $fish_color_status) (set_color --bold $fish_color_status) \
#    $last_pipestatus)
#
#    set -l textcol green
#    set_color $textcol
#    printf $USER
#    set -l textcol yellow
#    set_color $textcol
#    printf @
#    set -l textcol blue
#    set_color $textcol
#    printf (hostname)
#    set -l textcol yellow
#    set_color $textcol
#    printf 🦈:
#    printf (dirs)
#    # print last status
#    printf " %s" $mystatus
#    set -l textcol white
#    set_color $textcol
#    printf '\n$ '
#end
### bat as manpager
set --export MANROFFOPT "-c"
set --export MANPAGER "sh -c 'col -bx | batcat -l man -p'"
source $HOME/.config/fish/alias.fish
set --export SUDO_EDITOR nvim
### starship prompt
starship init fish | source
