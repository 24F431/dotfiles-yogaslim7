function cwebp
  for img in (find $PWD -iname '*.webp')
    if convert $img (echo $img | sed 's/\.[^.]*$//').jpg
      echo ✅ $img konvertiert
    else
      echo ❌ $img nicht konvertiert
    end
  end
end
