# vim:ft=fish
# Alias definitions.
alias mydate='date +%F_%H-%M-%S'
alias desk='cd $HOME/Schreibtisch/'
alias op='opera --private'
alias cls='clear'
alias xclip="xclip -selection c"
alias catc='highlight --out-format=ansi'
alias hl='highlight --out-format=ansi'
alias hlsh='highlight --out-format=ansi --syntax=sh'
alias dff='df -h --exclude-type=squashfs --exclude-type=tmpfs | batcat'
alias c='clear'
alias pi2='ssh pi@192.168.178.46'
alias r='ranger'
alias ts='printf "%sx%s\n" (tput cols) (tput lines)'
alias smeth='speedometer -r wlp1s0'
alias mylsblk='lsblk --exclude 7'
# alias vali='vim $HOME/.bash_aliases'
alias mygit='/usr/bin/git --git-dir=$HOME/dotfiles-yogaslim7 --work-tree=$HOME'
alias emoji-clip='eval $HOME/myscript/emoji-clip.sh'
alias mydatex="date +%F_%H-%M-%S | tr -d '\n' | tee (tty) | xclip"
alias name-dir='eval $HOME/myscript/name-dir.sh'
alias color='eval $HOME/myscript/color/color.sh'
#alias cpufetch='/home/raf/myscript/cpufetch'
# abbreviations definitions
abbr myip 'ip -4 -c addr'
abbr inetip 'curl ipinfo.io/ip'
abbr ai "sudo apt install -y"
abbr ac "sudo apt clean"
abbr as "apt search"
abbr nu "sudo nala upgrade"
abbr jd '~/myscript/jd.sh'
abbr pi2 'ssh pi@pi2.fritz.box'
abbr cppi2 'scp pi@pi2.fritz.box:'
abbr akt '$HOME/myscript/update.sh'
abbr scanc "nmap -sn 192.168.178.0/24 | egrep --color '([0-9]{1,3}\.){3}[0-9]{1,3}|\$'"
abbr neocpu "neofetch && cpufetch | grep -v '^\s*\$' | grep -v '^\s*.\[m\$'"
abbr kal 'ncal -3wb'
abbr yF "youtube-dl -F '"
abbr yf "youtube-dl -f 137 '"
abbr tn 'tmux new-session -s 󰯉 -n  \; new-window -n \; new-window -n 󰐹\; select-window -t 1'
abbr ta 'tmux attach'
abbr el 'eza --icons --group-directories-first --header -F -l'
abbr ef 'eza --icons --group-directories-first -F'
abbr lvim '~/myscript/lvim.sh'
### wireguard
abbr wgup 'wg-quick up RafYoga7-PC'
abbr wgdown 'wg-quick down RafYoga7-PC'
abbr ZZ exit
abbr ndr "mpv 'https://icecast.ndr.de/ndr/ndr2/niedersachsen/mp3/128/stream.mp3'"
### cryfs
abbr mycrypt '~/myscript/mycrypt.sh'
#abbr cwebp "for img in (find $PWD -iname '*.webp'); convert $img (echo $img | sed 's/\.[^.]*\$//').jpg; end"
# To bind Ctrl-O to ranger-cd, save this in `~/.config/fish/config.fish`:
bind \co ranger-cd
